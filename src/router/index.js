import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import Product from '../views/Product'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
 
  {
    path: '/login',
    component: Login
  },
  {
    path: '/register',
    component: Register
  },
  
  {
    path: '/Customer',
    name: 'Customer',
    component:()=> import('../views/Customer')
  },
  {
    path: '/table',
    name: 'table',
    component:()=> import('../views/Table.vue')
  },
  {
    path: '/user',
    name: 'user',
    component:()=> import('../views/User')
  },
  

  {
    path: '/product',
    name: 'product',
    component:()=> import('../views/Product')
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
